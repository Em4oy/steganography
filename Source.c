#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Zadavane na rezim (mode) - nacalno (0), kodirane (1), dekodirane (2)
int mode = 0;
// Bazova kartina, Kartinka sas sobshtenie, Izhoden fail sys sobshtenie
FILE* in_img, * out_img, * out_msg;
// Bazova kartina (bmp) pat, Izhodna kartina (bmp) sys sobshtenie v neia pat, Izhoden fail (txt) sys sobshtenie pat, Sobshtenie za zapisvane (max 255, 8bit 0 - 255, 0 ne e validno zashtoto sobshtenie ne moze da e prazno s 0 dalzina)
char in_pimg[100] = "", out_pimg['40'] = "D:\\Emo123\\Project1\\x64\\Debug\\out-pic.bmp", out_pmsg[100] = "", in_msg[255] = "";

/* Bitmap file header (14 bytes) - https://en.wikipedia.org/wiki/BMP_file_format */
int h_offset(FILE* inimg) {
	fseek(inimg, 14, 0);
	return (int)fgetc(inimg);
}

// Dalzina na textovo syobshtenie
int inMsgLength(char m[]) {
	int mc;
	// Broi dokato ne e dostignat krai na string
	for (mc = 0; m[mc] != '\0'; ++mc);
	return mc;
}

// bit ot bytes
int getBit(char b, int p) {
	return((b >> 8 - p) & 1);
}

// Kodirane na sobshtenie v kartiana
char encodeMsg() {
	in_img = fopen(in_pimg, "rb");
	if (in_img == NULL) {
		return (printf("Ne moze da se otvori vhodiasha kartina: %s\n", in_pimg));
	}

	out_img = fopen(out_pimg, "wb");
	if (out_img == NULL) {
		return (printf("Ne moze da se sazdade kartina za zapis %s\n", out_pimg));
	}

	// Header informacia (otstapka/pozicia) - bazova kartina
	char b_header;
	int hoffset = h_offset(in_img);

	// Postaviane na pozicia start
	rewind(in_img);

	// Kopirane na header ot bazova kartina v izhodana kartina
	for (int i = 0; i < hoffset; i++) {
		b_header = fgetc(in_img);
		fputc(b_header, out_img);
	}

	char img_char, msg_char;

	// Dalzina na sobshtenieto
	int mlength = inMsgLength(in_msg);

	// Zapis na dalzina na sobshtenie - sled header
	fputc(mlength, out_img);

	fseek(in_img, 14, SEEK_CUR);
	fseek(out_img, 13, SEEK_CUR);

	int char_i = 0;
	int msg_walk = 1;

	do {
		int msg_bit;
		// Proverka dali e krai na fail - 0 ako ne e
		if (msg_walk == 1) {
			for (int k = 0; k < mlength; k++) {
				// Byte ot sobshtenie
				msg_char = in_msg[k];
				for (int i = 1; i <= 8; i++) {
					// Vseki bit vav vseki byte ot katina
					img_char = fgetc(in_img);
					int lsb_bit = img_char & 1;
					msg_bit = getBit(msg_char, i);
					if (lsb_bit == msg_bit) {
						fputc(img_char, out_img);
					}
					else {
						if (lsb_bit == 0)
							img_char = (img_char | 1);
						else
							img_char = (img_char & 0);
						fputc(img_char, out_img);
					}
				}
			}
			msg_walk = 0;
		}
		else {
			// Zapis na vsichko ostanalo sled sobshtenie ot bazova kartuna v kartina sas sobshtenie
			char_i = fgetc(in_img);
			fputc(char_i, out_img);
		}
	} while (!feof(in_img)); // Proverka za krai na bazova kartina/potok

	fclose(in_img);
	fclose(out_img);
	return (printf("Sobshtenieto e postaveno v kartina: %s\n", out_pimg));
}

// Decodirane na sobshtenie v kartiana
char decodeMsg() {
	out_img = fopen(out_pimg, "rb");
	if (out_img == NULL) {
		return (printf("Ne moze da se otvori kartina sas sobshtenie: %s\n", out_pimg));
	}

	out_msg = fopen(out_pmsg, "w");
	if (out_msg == NULL) {
		return (printf("Ne moze da se sazdade fail za zapis na procheteno sobshtenie: %s\n", out_pmsg));
	}

	int hoffset = h_offset(out_img);
	// Pozicia sled header
	fseek(out_img, hoffset, SEEK_SET);

	// Vzimane na dalzina na sobshtenie sled header
	int message_length = (int)fgetc(out_img);

	char img_char;

	// Pozicia na parvi pixel
	fseek(out_img, 13, SEEK_CUR);
	for (int i = 0; i < message_length; i++) {
		char null_chr = '\0';
		// Prochitane i postaviane na sobshtenie v txt fail
		for (int j = 0; j < 8; j++) {
			null_chr = null_chr << 1;
			img_char = fgetc(out_img);
			int lsb_bit = img_char & 1;
			null_chr |= lsb_bit;
		}
		fputc(null_chr, out_msg);
	}

	fclose(out_img);
	fclose(out_msg);
	return (printf("Sobshtenieto e procheteno ot kartina i postaveno v: %s\n", out_pmsg));
}

// Pokazvane na pomoshtna informacia/menu v konzola
void helpInfo() {
	printf("Komandai za postaviane na sobshtenie v kartina:\n");
	printf("i - vhodiasha kartina *.bmp (max 100 simvola)\n");
	printf("o - fail za dekodirano saobshtenie *.txt (max 100 simvola)\n");
	printf("m - textovo sobshtenie za kodirane (max 255 simvola)\n");
	printf("e - kodirane na sobshtenie v kartiana\n");
	printf("d - dekodirane na sobshtenie ot kartiana\n");
	printf("q - izhod\n");
}

int main(int argc) {
	// Komanda ot konzola - char (nachalno (h) pomosht)
	char inchr = 'h';
	do {
		printf("\n");
		switch (inchr) {
		case 'i':
			// Zadavane na ime na fail ot konzola i zapazvane v in_pimg
			printf("Vhodiasha kartina (bmp):\n");
			gets(in_pimg);
			break;
		case 'o':
			// Zadavane na ime na fail ot konzola i zapazvane v in_msg
			printf("Fail za dekodirano saobshtenie (txt):\n");
			gets(out_pmsg);
			break;
		case 'm':
			printf("Textovo sobshtenie za kodirane:\n");
			gets(in_msg);
			break;
		case 'e':
			mode = 1;
			if (strlen(in_pimg) == 0) {
				mode = 0;
				printf("Ne e posochena vhodiasha kartina (bmp)!\n");
			}
			if (strlen(in_msg) == 0) {
				mode = 0;
				printf("Ne e zadadeno textovo sobshtenie za kodirane!\n");
			}
			if (mode == 1) {
				// Kodirane na sobshtenie v kartiana
				encodeMsg();
			}
			break;
		case 'd':
			mode = 2;
			if (strlen(out_pmsg) == 0) {
				mode = 0;
				printf("Ne e posochen fail za dekodirano saobshtenie (txt)!\n");
			}
			if (mode == 2) {
				// Dedirane na sobshtenie v kartiana
				decodeMsg();
			}
			break;
		case 'h':
			helpInfo();
			break;
		}
		inchr = getche();
	} while (inchr != 'q');

	return 0;
}